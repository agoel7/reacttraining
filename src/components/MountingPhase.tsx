import { log } from 'console';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

interface Props {
  favoritefood: string;
}

interface State {
  favoritefood: string;
}

class MountingPhase extends Component<Props, State> {
  constructor(props: Props) {
    console.log(1);
    super(props);
    this.state = { favoritefood: 'rice' };
  }

  componentDidMount() {
    console.log(4);
    setTimeout(() => {
        console.log("Inside Settimeout");
        this.setState({ favoritefood: "pizza" });
    }, 5000);
  }

  static getDerivedStateFromProps(props: Props, state: State) {
    console.log(2);
    
    if (props.favoritefood !== state.favoritefood) {
        console.log('D',props.favoritefood);
        console.log('D',state.favoritefood);

      return { favoritefood: state.favoritefood };
    }
    return null;
  }

  render() {
    console.log(3, this.state.favoritefood);
    return <h1>My Favorite Food is {this.state.favoritefood}</h1>;
  }
}

export default MountingPhase