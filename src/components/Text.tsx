import React from 'react';
type Props = {
    message: string
}

export default function TextComponent(props: Props){
    return (
        <>
        <h1>The message is {props.message}</h1>
        </>
    )
}