import React, { Dispatch } from "react";

type Props = {
    placeHolder: string;
    handleChange: (input: string) => void;
    setValue: Dispatch<string>
}

const InputComponent = (props: Props)=>{
    return (<>
    <input placeholder={props.placeHolder} onChange={(e) => props.setValue(e.target.value)}  />
    </>)

}

export default InputComponent