import React from "react";

const WithoutJSXComponent = () => {
    return React.createElement('div',null, [
        React.createElement('h1',null,"Hello React without JSX"),
        React.createElement('p',null,'This is an example without the use of JSX')
    ])
}

export default WithoutJSXComponent