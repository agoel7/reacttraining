import React, { ReactComponentElement } from "react";

interface Props {
    value1: string;
    value2: number;
    value3: boolean;
    value4: () => void;
    value5: object;
    value6: number[]
    value7: React.ReactNode
}


const ValuesAsPropsComponent = (props: Props) => {
    return (<></>)

}

export default ValuesAsPropsComponent