import React, { Component } from 'react';

interface Props {
  favoritefood: string;
}

interface State {
  favoritefood: string;
}

class Header extends Component<Props, State> {
  constructor(props: Props) {
    console.log(1);
    super(props);
    this.state = { favoritefood: "rice" };
  }

  componentDidMount() {
    console.log(2);
    setTimeout(() => {
      this.setState({ favoritefood: "pizza" });
    }, 5000);
  }

  getSnapshotBeforeUpdate(prevProps: Props, prevState: State) {
    console.log(3);
    document.getElementById("div1")!.innerHTML =
      "Before the update, the favorite was " + prevState.favoritefood;
  }

  componentDidUpdate() {
    console.log(4);
    document.getElementById("div2")!.innerHTML =
      "The updated favorite food is " + this.state.favoritefood;
  }

  render() {
    console.log(5);
    return (
      <div>
        <h1>My Favorite Food is {this.state.favoritefood}</h1>
        <div id="div1"></div>
        <div id="div2"></div>
      </div>
    );
  }
}

export default Header;
