import React, { Component } from 'react';

class UnMount extends Component {
  componentDidMount() {
    console.log(2);
    console.log('Component mounted');
  }

  componentWillUnmount() {
    console.log(3);
    console.log('Component will unmount');
    // Perform cleanup tasks here
  }

  render() {
    console.log(1);
    return <h1>Hello, World!</h1>;
  }
}

export default UnMount;
