import React from "react";

type Props = {
    onclickFunction: () => void
}

export default function ButtonComponent(props: Props){
    return (
        <>
        <button onClick={props.onclickFunction}>Button</button>
        </>
    )
}