import React, { useState } from "react";
import logo from "./logo.svg";
import "./App.css";
import TextComponent from "./components/Text";
import ButtonComponent from "./components/Button";
import InputComponent from "./components/Input";
import WithoutJSXComponent from "./components/WithoutJSX";
import ClassCounter from "./components/classBased";
import ValuesAsPropsComponent from "./components/ValuesAsProps";
import KeysComponent from "./components/keys";
import MountingPhase from "./components/MountingPhase";
import UpdatePhase from "./components/UpdatingPhase";
import UnMount from "./components/Unmounting";

function App() {
  const [value, setValue] = useState("No-value");
  const [inputValue, setInputValue] = useState("0");
  const onclick = () => {
    setInputValue(value);
  };
  const handleChange = (inputValue: string) => {
    setValue(inputValue);
  };
  const demoFunction = () => {};

  return (
    <>
      {/* <h1>Different Components:</h1>
      <ValuesAsPropsComponent
        value1="string"
        value2={10}
        value3={true}
        value4={demoFunction}
        value5={{ name: "Atharv" }}
        value6={[1,2,3,4,5]}
        value7={<WithoutJSXComponent />}
      />
      <p>
        <InputComponent
          placeHolder="Enter Text"
          handleChange={() => handleChange(value)}
          setValue={setValue}
        />
      </p>
      <p>
        <ButtonComponent onclickFunction={onclick} />
      </p>
      <TextComponent message={inputValue} />
      <p>
        <WithoutJSXComponent />
      </p>
      <p>
        <ClassCounter />
      </p>
      <p>
        <KeysComponent />
      </p> */}
      {/* <div>
        <MountingPhase favoritefood="rice"/>
      </div> */}
      {/* <div>
        < UpdatePhase favoritefood="rice"/>
      </div> */}
      <div>
        <UnMount />
      </div>
    </>
  );
}

export default App;
